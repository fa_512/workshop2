<?php
$recordTempe = array(
    78, 60, 62, 68, 71, 68, 73, 85, 66, 64,
    76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73
);
//print_r($recordTempe);
$resultados = array_unique($recordTempe); // elimina los elemntos duplicados y devuelve un array limpio


$suma = 0;
//suma las temperaturas
foreach ($resultados as $resultado) {
    # code...
    $suma += $resultado;
}
//obtiene la cantidad de elementos de un array
$cantidadElem = count($resultados);

$promed = round($suma / $cantidadElem, 2); // la funcion round se usa para redondear decimales
echo "Average Temperature is: $promed <br><br>";

//List of 5 lowest temperatures (no duplicates) : 60, 62, 63, 64, 65  
sort($resultados); //ordena de forma ascendente
$fiveLowest = array_slice($resultados, 0, 5); // secciona una parte del arreglo de acuerdo a los parametros que ingrese(5 temperaturas)
echo "The five lowest temperatures are: ";
echo "<pre>";
print_r($fiveLowest);
echo " <pre>";

//List of 5 highest temperatures (no duplicates) : 76,78,79,81,85
rsort($resultados); //ordena de forma descendente
$fiveHighest = array_slice($resultados, 0, 5);
echo "The five highest temperatures are: ";
echo "<pre>";
print_r($fiveHighest);
echo " <pre>";
